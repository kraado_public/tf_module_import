########################
# create vpc

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = merge(local.tags, tomap({"Name" = var.vpc_name}))

}

########################
# create igw
resource "aws_internet_gateway" "igw" {
  count = var.igw == true ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = merge(local.tags, tomap({"Name" = "${var.vpc_name}-igw"}))

}

########################
# Create public_subnets subnet
resource "aws_subnet" "public_subnets" {
  for_each = local.public_subnets
  vpc_id = aws_vpc.vpc.id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
}


# Create private_subnets subnet
resource "aws_subnet" "private_subnets" {
  for_each = local.private_subnets
  vpc_id = aws_vpc.vpc.id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
}

# Create database_subnets subnet
resource "aws_subnet" "database_subnets" {
  for_each = local.database_subnets
  vpc_id = aws_vpc.vpc.id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
}


# Create cache_subnets subnet
resource "aws_subnet" "cache_subnets" {
  for_each = local.cache_subnets
  vpc_id = aws_vpc.vpc.id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
}
