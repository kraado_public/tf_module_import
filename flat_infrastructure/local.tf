locals {
  default_tags = {
    IACTool      = "terraform"
    Environment  = var.environment
  }

  tags            = merge(local.default_tags, var.tags)
}


// SUBNET AND AZ

locals {
  public_subnets = {
    mkc_pub_subnet_1 = ["172.20.253.0/24", "${var.region}a"]
    mkc_pub_subnet_2 = ["172.20.254.0/24", "${var.region}b"]
  }

  private_subnets = {
    mkc_priv_subnet_1 = ["172.20.0.0/24", "${var.region}a"]
    mkc_priv_subnet_2 = ["172.20.1.0/24", "${var.region}b"]
  }

  database_subnets = {
    mkc_database_subnet_1 = ["172.20.10.0/24", "${var.region}a"]
    mkc_database_subnet_2 = ["172.20.11.0/24", "${var.region}b"]
  }

  cache_subnets = {
    mkc_cache_subnet_1 = ["172.20.15.0/24", "${var.region}a"]
    mkc_cache_subnet_2 = ["172.20.16.0/24", "${var.region}b"]
  }

}