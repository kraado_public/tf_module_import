output "vpc_details" {
  value = {
    vpc_id = aws_vpc.vpc.id
    vpc_arn = aws_vpc.vpc.arn
    igw_id = var.igw == true ? aws_internet_gateway.igw[0].id : null
  }
}

output "public_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.public_subnets : k => sub.id
  }
}


output "private_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.private_subnets : k => sub.id
  }
}


output "database_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.database_subnets : k => sub.id
  }
}


output "cache_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.cache_subnets : k => sub.id
  }
}
