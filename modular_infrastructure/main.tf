########################
# create vpc

/* resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = merge(local.tags, tomap({"Name" = var.vpc_name}))

}

########################
# create igw
resource "aws_internet_gateway" "igw" {
  count = var.igw == true ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = merge(local.tags, tomap({"Name" = "${var.vpc_name}-igw"}))

} */


module "vpc" {
  source = "./modules/vpc_v2"
  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
  tags = var.tags
}



########################
# Create public_subnets subnet
/* resource "aws_subnet" "public_subnets" {
  for_each = local.public_subnets
  vpc_id = module.vpc.vpc_details.vpc_id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
} */

module "public_subnets" {
  source = "./modules/subnets_v2"
  vpc_id = module.vpc.vpc_details.vpc_id
  subnets = local.public_subnets
  tags = var.tags
}

# Create private_subnets subnet
/* resource "aws_subnet" "private_subnets" {
  for_each = local.private_subnets
  vpc_id = module.vpc.vpc_details.vpc_id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
} */

module "private_subnets" {
  source = "./modules/subnets_v2"
  vpc_id = module.vpc.vpc_details.vpc_id
  subnets = local.private_subnets
  tags = var.tags
}

# Create database_subnets subnet
/* resource "aws_subnet" "database_subnets" {
  for_each = local.database_subnets
  vpc_id = module.vpc.vpc_details.vpc_id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
} */

module "database_subnets" {
  source = "./modules/subnets_v2"
  vpc_id = module.vpc.vpc_details.vpc_id
  subnets = local.database_subnets
  tags = var.tags
}

# Create cache_subnets subnet
/* resource "aws_subnet" "cache_subnets" {
  for_each = local.cache_subnets
  vpc_id = module.vpc.vpc_details.vpc_id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
} */

module "cache_subnets" {
  source = "./modules/subnets_v2"
  vpc_id = module.vpc.vpc_details.vpc_id
  subnets = local.cache_subnets
  tags = var.tags
}