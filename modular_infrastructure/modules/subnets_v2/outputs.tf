/* // output the following details of every subnet
output "subnet_outputs" {
  description = ""
  value = {
    for k, sub in aws_subnet.subnet : k => {
    id                = sub.id
    availability_zone = sub.availability_zone
    }
  }
}
 */

// output the following details of every subnet
output "subnet_outputs" {
  description = ""
  value = {
    for k, sub in aws_subnet.subnet : k => sub.id
  }
}
