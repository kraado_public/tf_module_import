# Create subnet
resource "aws_subnet" "subnet" {
  for_each = var.subnets
  vpc_id = var.vpc_id
  cidr_block = each.value[0]
  availability_zone = each.value[1]

  tags = merge(local.tags, tomap({"Name" = each.key}))
}




