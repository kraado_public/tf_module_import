variable "vpc_id" {
  description = "vpc_id"
}

variable "subnets" {
  description = ""
  type = map(list(string))
  # default = {
  #   "subnet1_name" = [ "cidr_value", "az" ]
  #   "subnet2_name" = [ "cidr_value", "az" ]
  # }
}


variable "tags" {
  description = "email address of resource creator"
  type = map(string)
#  default = null
}


variable "environment" {
  type = string
  description = "prod or staging"
  default = null
}
