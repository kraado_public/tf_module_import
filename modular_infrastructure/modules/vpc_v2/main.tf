########################
# create vpc

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = merge(local.tags, tomap({"Name" = var.vpc_name}))

}

########################
# create igw
resource "aws_internet_gateway" "igw" {
  count = var.igw == true ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = merge(local.tags, tomap({"Name" = "${var.vpc_name}-igw"}))

}





