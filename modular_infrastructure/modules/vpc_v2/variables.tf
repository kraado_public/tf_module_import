// VPC DETAILS

variable "vpc_cidr" {
  description = "the VPC cidr block"
  type = string
}

variable "vpc_name" {
  description = "the VPC cidr block"
  type = string
}

variable "igw" {
  description = "to create IGW"
  type = bool
  default = true
}



// TAGS

variable "environment" {
  type = string
  description = "prod or staging"
  default = null
}

variable "tags" {
  description = "Please supply the Environment key at least. Name will be supplied here."
  type        = map(string)
  # default     = null
}

