output "vpc_details" {
  value = {
    vpc_id = module.vpc.vpc_details["vpc_id"]
    vpc_arn = module.vpc.vpc_details["vpc_arn"]
    igw_id = module.vpc.vpc_details["igw_id"]
  }
}

output "public_subnets" {
  description = ""
  value = module.public_subnets.subnet_outputs
}

output "private_subnets" {
  description = ""
  value = module.private_subnets.subnet_outputs
}

output "database_subnets" {
  description = ""
  value = module.database_subnets.subnet_outputs
}

output "cache_subnets" {
  description = ""
  value = module.cache_subnets.subnet_outputs
}

/* output "public_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.public_subnets : k => sub.id
  }
}


output "private_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.private_subnets : k => sub.id
  }
}


output "database_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.database_subnets : k => sub.id
  }
}


output "cache_subnets" {
  description = ""
  value = {
    for k, sub in aws_subnet.cache_subnets : k => sub.id
  }
} */
