
// VPC VARIABLES
variable "vpc_name" {
  default = "mkc_lab_vpc"
}

variable "vpc_cidr" {
  default = "172.20.0.0/16"
}

variable "region" {
  default = "eu-west-1"
}

variable "igw" {
  description = "to create IGW"
  type = bool
  default = true
}


variable "environment" {
  type = string
  description = "prod or staging"
  default = null
}

variable "tags" {
    default = {
        Created_by = "mkc"
    }
  
}

